## Tutorials

1. Python packages
* [Omitted Variable Bias](05_tutorial_riskareg_library.ipynb)
* [Statistical Parity Bias](06_tutorial_statparity_library.ipynb)
* [Simpson's Paradox](07_tutorial_simpsons_paradox_library.ipynb)

2. How to approach fairness in the ML pipeline, fairness/bias tradeoffs
* [Allstate Dataset – Logistic Regression](01_tutorial_allstate_logistic-regression.ipynb)
* [Allstate Dataset – Decision Tree](02_tutorial_allstate_decision-tree.ipynb)
* [German Dataset – Logistic Regression](03_tutorial_german_logistic-regression.ipynb)
* [German Dataset – Decision Tree](04_tutorial_german_decision-tree.ipynb)


If you find any bugs, please open an issue here: [Issue](https://gitlab.oit.duke.edu/mids-capstone-projects/2020-2021/debiasing-datasets/-/issues/new)

### Links
[IBM AI Fairness 360](https://aif360.readthedocs.io/en/latest/)