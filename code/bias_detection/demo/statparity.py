#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: jdavidmartinezg
"""

import pandas as pd
import numpy as np
import json
from aif360.datasets import BinaryLabelDataset
from aif360.metrics import BinaryLabelDatasetMetric, ClassificationMetric
from aif360.explainers import MetricTextExplainer, MetricJSONExplainer

from aif360.algorithms.preprocessing.reweighing import Reweighing

from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import accuracy_score

from IPython.display import Markdown, display
from tqdm import tqdm
import warnings
warnings.filterwarnings('ignore')


class statparity:

    def __init__(self, dataset):

        self.dataset = dataset

    def report(self):

        results = {}
        list_of_vars = []

        for var in self.dataset.protected_attribute_names:
            privileged_groups = []
            unprivileged_groups = []

            privileged_groups.append({var: 0})
            unprivileged_groups.append({var: 1})

            metrics = BinaryLabelDatasetMetric(self.dataset,
                                               unprivileged_groups=unprivileged_groups,
                                               privileged_groups=privileged_groups)

            json_explainer = MetricJSONExplainer(metrics)

            mean_diff = json.loads(json_explainer.mean_difference())
            di = json.loads(json_explainer.disparate_impact())
            # NOTE: json_explainer.consistency() has a bug (https://github.com/Trusted-AI/AIF360/issues/227)
            consist = metrics.consistency(n_neighbors=5)[0]
            # create array to manage around bug in source code
            consist_d = {'metric': 'Consistency',
                         'message': 'Consistency (measures how similar labels are for similar instances): '+str(consist)}

            results[var] = [mean_diff, di, consist_d]
            list_of_vars.append(var)

        df = self.make_results_table(results, list_of_vars)
        return df[['Name', 'Mean Difference']]

    def make_results_table(self, results, list_of_vars):

        def expand_metric(df, metric, list_of_vars):
            # expand dictionary column into dataframe
            df = df[metric].apply(pd.Series)
            df['name'] = list_of_vars
            # get metric value
            df['message'] = df['message'].astype(str)
            df[metric] = df['message'].apply(lambda x: x.split(':')[1])
            # keep relevant columns
            df = df[['name', metric]].copy()
            df[metric] = df[metric].astype(float).round(2)
            return df

        # convert json into dataframe
        df_results = pd.DataFrame.from_dict(results, orient='index').reset_index().rename(columns={'index': 'Feature',
                                                                                                   0: 'Mean Difference',
                                                                                                   1: 'Disparate Impact',
                                                                                                   2: 'Consistency'})
        # get column names
        list_of_metrics = df_results.columns[1:].tolist()
        df0 = pd.DataFrame(np.zeros((len(list_of_vars), 1)))

        for metric in list_of_metrics:
            df = expand_metric(df_results, metric, list_of_vars)
            df0 = pd.concat([df0, df], axis=1)

        # get variable names
        vars_col = df0['name'].iloc[:, 0]
        df0.drop(columns=['name', 0], inplace=True)
        df0['Name'] = vars_col
        df0 = df0[['Name', 'Mean Difference', 'Disparate Impact', 'Consistency']].copy()
        return df0



