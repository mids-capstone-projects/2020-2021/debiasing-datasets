#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan  6 20:18:11 2021

@author: jdavidmartinezg
"""

import os
import pandas as pd
import numpy as np
import statsmodels.api as sm 
import random


class riskareg:

    def __init__(self, df, risk_var, decision_var, covariates):
        """     
    
        Parameters
        ----------
        df : Pandas dataframe 
            Dataframe with features to evaluate. There should be at least 3 columns:
            one for the risk, another for the binary decision and at least one covariate.
        
        risk_var: String signalling the name of the column in df containing information regarding risk.

        decision_var: String signalling the name of the column in df containing information regarding decision to be evaluated in fairness.

        covariates : Python list containing the names of the columns to use for the risk estimation.

        Returns
        -------
        None.

        """
        self.df = df
        self.risk_var = risk_var
        self.decision_var = decision_var
        self.covariates = covariates

    def __risk_estimate(self):
        """
        
        Risk estimate for each individual of the dataset based on chosen covariates.
        
        Returns
        -------
        risk_vector : numpy array containing the risk estimate for each individual of the dataset.
        """        
        
        risk_reg = sm.Logit(self.df[self.risk_var], self.df[self.covariates]).fit() 
        
        ypred = risk_reg.predict(self.df[self.covariates])
        
        risk_vector = np.array(ypred)
        
        return risk_vector
    
    
    def risk_adj_disparity(self, protected_attribute):
        """     
    
        Parameters
        ----------
        protected_attribute : Variable containing information for protected attribute to be evaluated for disparate impact.

        Returns
        -------
        None.

        """       
        y = self.df[self.decision_var]
        
        x = pd.concat([pd.DataFrame(self.__risk_estimate()), self.df[protected_attribute]], axis = 1)
        
        y = y.reindex(x.index)
        
        x.rename(columns = {0:'risk_control_estimate'}, inplace = True)
         
        risk_diparity_reg = sm.Logit(y, x).fit() 
        
        print(risk_diparity_reg.summary()) 
        
    def data_sample(n_samples = 200): 
        
        risk = [random.randint(0,1) for i in range(n_samples)]    

        decision = [random.randint(0,1) for i in range(n_samples)]   

        a = [random.randint(0,1) for i in range(n_samples)]  

        b = [random.randint(0,1) for i in range(n_samples)]  

        c = [random.random() for i in range(n_samples)]   

        d = [random.random() for i in range(n_samples)]   

        sample_data = {'risk':risk,
                       'decision':decision,
                       'a':a,
                       'b':b,
                       'c':c,
                       'd':d}

        sample_df = pd.DataFrame(sample_data)

        return sample_df

# .............................................................................
