# De-biasing Datasets: Investigating Bias Detection, Mitigation and Accuracy/Bias Tradeoffs

This repository includes code related to the capstone's milestones and deliverables.

## Project Summary

In this project, we developed methods for evaluating fairness and correcting for discovered biases in datasets. We studied these preprocessing methods in the context of multiple datasets and developed a systematic way of identifying and mitigating the bias that exists in the pre-modeling stage. Specifically, we have created a set of tools that will allow practitioners to analyze datasets in terms of bias and determine whether in the pre-processing stage of data-related projects there should be a focus on mitigating these effects. These tools include Python packages that detect different types of biases, and jupyter notebook tutorials that show how to mitigate biases in datasets.

## Goals

* Developed Python packages to detect biases in datasets (i.e. Omitted Variable Bias, Statistical Parity Bias, and Simpson’s Paradox) (see [tutorials](tutorials))
* Developed tutorial to guide data scientists on how to approach fairness in their work for multiple sensitive attributes (see [tutorials](tutorials))
* Visualized tradeoffs between model parameters, accuracy, and fairness metrics to help make informed decisions (see [tutorials](tutorials))

### Project members:

Project leader:

* Ezinne Nwankwo (IGE Fellow)  
Institution: Duke University - Department of Statistical Science \
Email: ezinne dot nwankwo at duke dot edu

MIDS Team:

* Abdur Rehman  
Institution: Duke University - Social Science Research Institute (SSRI) \
Email: abdur dot rehman at duke dot edu

* Juan David Martinez Gordillo  
Institution: Duke University - Social Science Research Institute (SSRI) \
Email: jdavid dot martinez at duke dot edu

Faculty advisor:

* Allison Chaney (Faculty Mentor)  
Institution: Duke University - The Fuqua School of Business \
Email: allison dot chaney at duke dot edu

## Capstone Reports

- [Link](https://docs.google.com/document/d/1w-sorwDGk7Vlw51a5rv9ur6cI3Pu7u0lOv6GfhkNTFw/edit?usp=sharing) to final report (Spring 2021).

- [Link](https://docs.google.com/document/d/1IK_lnp5fvr0NUi0pwqdqR4ppBvqsxJplbmtWvPi0c4Y/edit?usp=sharing) to capstone report (Fall 2020).


